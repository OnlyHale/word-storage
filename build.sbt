ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.2.0"

lazy val root = (project in file("."))
  .settings(
    name := "word-storage",
    resolvers += "mvn proxy" at "https://nexus.tcsbank.ru/repository/mvn-maven-proxy",
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.2.14" % Test,
      "dev.zio" %% "zio" % "2.0.3",
      "dev.zio" %% "zio-streams" % "2.0.3",
      "dev.zio" %% "zio-test" % "2.0.3" % Test,
      "dev.zio" %% "zio-test-sbt" % "2.0.3" % Test,
      "dev.zio" %% "zio-test-magnolia" % "2.0.3" % Test)
  )

scalacOptions ++= Seq("-Werror")