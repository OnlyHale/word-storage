package ru.tinkoff.backendacademy.wordstorage

import ru.tinkoff.backendacademy.wordstorage.backend.FileWordRepository
import ru.tinkoff.backendacademy.wordstorage.console.WordStorageConsoleFrontend
import zio._

import java.io.IOException
import java.nio.file.Files

object ConsoleInMemory extends ZIOAppDefault {
  override def run: ZIO[Any, IOException, Unit] = new WordStorageConsoleFrontend(
    new FileWordRepository(Files.createTempFile("backendacademy", null))
  ).start
}