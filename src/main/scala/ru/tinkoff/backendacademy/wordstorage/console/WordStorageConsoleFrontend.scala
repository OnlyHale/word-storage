package ru.tinkoff.backendacademy.wordstorage.console

import ru.tinkoff.backendacademy.wordstorage.backend.WordRepository
import zio._
import java.io.IOException

class WordStorageConsoleFrontend(backend: WordRepository) {

  private val commandAndWord = "^(get|put|delete)\\s+(.*)$".r

  private def Parser(request: String): ZIO[Any, IOException, Boolean] =
    request match
      case commandAndWord("get", word) =>
        Console.printLine(backend.get(word)).map(_ => true)
      case commandAndWord("put", word) =>
        backend.put(word)
        ZIO.succeed(true)
      case commandAndWord("delete", word) =>
        backend.delete(word)
        ZIO.succeed(true)
      case "quit" => ZIO.succeed(false)
      case _ => Console.printLine("Could not recognize command, please use one of " +
        """"get", "put" or "delete" and then write your word, or write "quit"""").map(_ => true)

  def start: ZIO[Any, IOException, Unit] = {
    for {
      request <- Console.readLine
      continue <- Parser(request)
      _ <- if (continue) start else ZIO.unit
    } yield ()
  }
}